#include <iostream>
#include <unordered_map>
#include <string>

using namespace std;

typedef unsigned int ui;
typedef unordered_map<ui, bool> umib;

bool kbit(ui x, ui k) {
	return (x & 1 << k) > 0;
}

void init_bads(umib& bad5, umib& bad6) {
	for (ui i = 0; i < 1 << 5; i++) {
		ui b0, b1, b3, b4;
		b0 = kbit(i, 0);
		b1 = kbit(i, 1);
		b3 = kbit(i, 3);
		b4 = kbit(i, 4);
		bad5[i] = (b0 == b4) && (b1 == b3);
	}

	for (ui i = 0; i < 1 << 6; i++) {
		ui b0, b1, b2, b3, b4, b5;
		b0 = kbit(i, 0);
		b1 = kbit(i, 1);
		b2 = kbit(i, 2);
		b3 = kbit(i, 3);
		b4 = kbit(i, 4);
		b5 = kbit(i, 5);
		bad6[i] = b0 == b5 && b1 == b4 && b2 == b3;
	}
}

bool fun(ui N, string& s, ui i, ui s5, ui s6, umib& bad5, umib& bad6) {
	if (bad5[s5] && i >= 5) return 0;
	if (bad6[s6] && i >= 6) return 0;
	if (i >= N) return 1;
	ui ns5, ns6;
	if (s[i] == '0' || s[i] == '?') {
		ns5 = s5 << 1;
		ns5 &= (1 << 5) - 1;
		ns6 = s6 << 1;
		ns6 &= (1 << 6) - 1;
		if (fun(N, s, i + 1, ns5, ns6, bad5, bad6)) return 1;
	}
	if (s[i] == '1' || s[i] == '?') {
		ns5 = s5 << 1;
		ns5 &= (1 << 5) - 1;
		ns5 += 1;
		ns6 = s6 << 1;
		ns6 &= (1 << 6) - 1;
		ns6 += 1;
		if (fun(N, s, i + 1, ns5, ns6, bad5, bad6)) return 1;
	}
	return 0;
}

int main() {
	umib bad5;
	umib bad6;
	init_bads(bad5, bad6);

	ui T, N;
	bool y;
	string s;

	cin >> T;
	for (ui x = 1; x <= T; x++) {
		cin >> N >> s;
		y = fun(N, s, 0, 0, 0, bad5, bad6);
		cout << "Case #" << x << ": " << (y ? "POSSIBLE" : "IMPOSSIBLE") << endl;
	}

	return 0;
}
