#include <iostream>
#include <string>

using namespace std;

typedef int ii;
typedef unsigned int ui;

string get_aux(ii n, ii count) {
	string aux = "";
	for (ii i = 0; i < count; i++) {
		if (n % 2 == 0) {
			aux = aux + "0";
		}
		else {
			aux = aux + "1";
		}
		n >>= 1;
	}
	return aux;
}

string get_test(string s, string aux) {
	ii i = 0;
	string test = "";

	for (auto c : s) {
		if (c == '?') {
			test = test + aux[i];
			i++;
		}
		else {
			test = test + c;
		}
	}

	return test;
}

bool is_pal(string test, ii i, ii n) {
	string aux = test.substr(i,n);
	for (ii j = 0; j < n/2; j++) {
		if (aux[j] != aux[n - j - 1]) {
			return false;
		}
	}

	return true;
}

bool has_pal(string test) {
	for (ui n = 5; n <= test.length(); n++) {
		for (ui i = 0; i <= test.length() - n; i++) {
			if (is_pal(test, i, n)) {
				return true;
			}
		}
	}

	return false;
}

bool fun(string s) {
	ii count = 0;
	for (int i = 0; i < s.length(); i++)
		count += s[i] == '?';

	for (int i = 0; i < (2 << count); i++) {
		string aux = get_aux(i, count);
		string test = get_test(s, aux);
		if (!has_pal(test)) {
			return true;
		}
	}

	return false;
}

int main() {
	ii T, N;
	string s;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		cin >> N >> s;
		ii y = fun(s);

		if (y) {
			cout << "Case #" << x << ": POSSIBLE" << endl;
		}
		else {
			cout << "Case #" << x << ": IMPOSSIBLE" << endl;
		}
	}

	return 0;
}
