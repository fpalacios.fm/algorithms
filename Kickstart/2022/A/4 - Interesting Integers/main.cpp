#include <iostream>

using namespace std;

typedef long long ll;
typedef int ii;

bool is_interesting(ll i) {
	ll p = 1;
	ll sum = 0;
	ll j = i;

	while (j > 0) {
		sum += j % 10;
		j /= 10;
	}

	while (i>0) {
		p *= i % 10;
		p %= sum;
		i /= 10;
	}

	return p%sum==0;
}

ll fun(ll A, ll B) {
	ll sum = 0;

	for (ll i = A; i <= B; i++) {
		sum += is_interesting(i);
	}

	return sum;
}

int main() {
	ii T;
	ll A, B;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		cin >> A >> B;
		ll y = fun(A, B);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
