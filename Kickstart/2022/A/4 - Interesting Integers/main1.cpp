#include <iostream>
#include <unordered_map>
#include <unordered_set>

using namespace std;

typedef long long ll;
typedef unordered_map<int, int> mii;
typedef unordered_set<ll> sl;
typedef int ii;

sl good;
sl bad;

void init_sum(ll A, mii& M, ll& sum) {
	while (A > 0) {
		M[A % 10]++;
		sum += A % 10;
		A /= 10;
	}
}

void update_sum(ll i, ll& sum, mii& M) {
	while (i > 0 && i % 10 == 0) {
		sum -= 9;
		M[9]--;
		M[0]++;
		i /= 10;
		if (M[9] <= 0) {
			M.erase(9);
		}
	}
	sum++;
	M[i % 10]++;

	if (!(i%10==1 && i<10)) {
		M[(i % 10) - 1]--;
	}

	if (M[(i % 10) - 1] <= 0) {
		M.erase((i % 10) - 1);
	}
}

bool is_interesting(ll i, mii& M, ll& sum) {
	if (good.count(i) > 0) return 1;
	if (bad.count(i) > 0) return 0;

	if (M[0] > 0) {
		good.insert(i);
		return 1;
	}

	ll p = 1;

	for (auto[digit, repetitions] : M) {
		if (digit <= 1) {
			continue;
		}
		for (int j = 1; j <= repetitions; j++) {
			p *= digit;
			p %= sum;
		}
	}

	if (p%sum == 0) {
		good.insert(i);
	}
	else {
		bad.insert(i);
	}
	return p % sum == 0;
}

ll fun(ll A, ll B) {
	ll count = 0, sum = 0, prod = 1;
	mii M;
	init_sum(A, M, sum);

	for (ll i = A; i <= B;) {
		count += is_interesting(i, M, sum);
		i++;
		update_sum(i, sum, M);
	}

	return count;
}

int main() {
	ii T;
	ll A, B;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		cin >> A >> B;
		ll y = fun(A, B);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
