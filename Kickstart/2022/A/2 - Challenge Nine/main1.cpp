#include <iostream>
#include <string>

using namespace std;

typedef int ii;

int main() {
	ii T, sum;
	string s;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		cout << "Case #" << x << ": ";
		cin >> s;
		sum = 0;

		for (auto c : s) {
			sum += c - '0';
			sum %= 9;
		}

		if (sum > 0) {
			sum = 9 - sum;
		}

		bool first = true;
		for (auto c : s) {
			if ((!first && sum==0) || (sum > 0 && c - '0' > sum)) {
				cout << sum;
				sum = -1;
			}
			cout << c;
			first = false;
		}

		if (sum >= 0) {
			cout << sum;
		}

		cout << endl;
	}

	return 0;
}
