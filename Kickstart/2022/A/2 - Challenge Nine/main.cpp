#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

typedef int ii;
typedef vector<char> vc;

void fun(vc& v, ii sum) {
	vc::iterator it = v.begin();
	if (sum > 0) {
		for (; it != v.end(); it++) {
			if (*it - '0' > sum) {
				cout << sum;
				break;
			}
			else {
				cout << *it;
			}
		}
		if (it == v.end()) {
			cout << sum;
		}
	}
	while (it != v.end()) {
		cout << *it;
		it++;
	}
}

int main() {
	ii T, sum;
	char c;
	vc v;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		cout << "Case #" << x << ": ";
		sum = 0;
		v = vc();
		do {
			cin >> c;
			v.push_back(c);
			sum += c - '0';
			sum %= 9;
		} while (cin.peek() != '\n');

		sum = (sum > 0)*(9 - sum);
		fun(v, sum);
		cout << endl;
	}

	return 0;
}
