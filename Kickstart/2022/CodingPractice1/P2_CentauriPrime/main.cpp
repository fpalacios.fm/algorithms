#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef vector<ll> vll;

bool isVowel(char c){
    return c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='I'||c=='O'||c=='U';
}

string printRuler(string s){
    char c = s.back();
    if(c=='y'||c=='Y'){
        return "nobody";
    }else if(isVowel(c)){
        return "Alice";
    }else{
        return "Bob";
    }
}

int main(){
    ll T;
    string s, ruler;
    cin >> T;

    for(ll i=1; i<=T; i++){
        cin >> s;
        ruler = printRuler(s);
        cout << "Case #" << i << ": " << s << " is ruled by " << ruler << ".\n";
    }

    return 0;
}
