#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef vector<ll> vll;

int main(){
    ll T, N, M, X, R=0;
    cin >> T;

    for(ll i=0; i<T; i++){
        cin >> N >> M;
        R = 0;

        for(ll i=0; i<N; i++){
            cin >> X;
            R = (R + X%M)%M;
        }

        cout << "Case #" << i+1 << ": " << R << endl;
    }

    return 0;
}
