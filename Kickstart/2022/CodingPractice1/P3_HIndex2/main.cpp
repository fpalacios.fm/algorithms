#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef map<ll, ll, greater<ll>> mll;

ll updateHi(mll mapin, ll M, ll last, ll hi){
    if(last<=hi){
        return hi;
    }

    ll count = 0, last_count, max = M;
    for(auto const& [key, val]: mapin){
        count+=val;

        if(key <= hi){
            last_count = count - val;
            return last_count <= hi ? hi : last_count;
        }else{
            M = key <= M ? key : M;
        }

        if(M<=hi){
            return hi;
        }
        if(count>=M){
            return M;
        }
    }

    return hi;
}

int main(){
    ll T, N, c, hi, x;
    mll mapin;

    cin >> T;

    for(ll i=1; i<=T; i++){
        cin >> N;
        c = 0, hi = 0;
        mapin = mll();

        cout << "Case #" << i << ":";

        for(ll j=0; j<N; j++){
            cin >> x;
            c++;
            mapin[x]++;
            hi = updateHi(mapin, c, x, hi);
            cout << ' ' << hi;
        }

        cout << endl;
    }

    return 0;
}
