#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef vector<ll> vll;
typedef map<ll, ll> mll;

ll updateHi(mll &mapin, ll &count, ll &ki, ll hi, ll x){
    if(ki==-1){
        ki = x;
    }

    if(x>=hi){
        mapin[x]++;
        count++;
        ki = min(ki, x);
    }else{
        return hi;
    }

    if(ki >= count && count > hi){
        hi = count;
    }

    auto it = mapin.find(ki);
    auto key = it->first;
    auto val = it->second;
    auto knext = next(it, 1)->first;
    while(it != mapin.end() && count - val > hi){
        count -= val;
        ki = knext;
        hi = min(ki, count);
        it++;
    }

    return min(count, ki);
}

int main(){
    ll T, N, count, hi, ki, x;
    mll mapin;

    cin >> T;

    for(ll i=1; i<=T; i++){
        cin >> N;
        count = 0, hi = 0, ki = -1;
        vll vout(N);
        mapin = mll();

        for(ll j=0; j<N; j++){
            cin >> x;
            hi = updateHi(mapin, count, ki, hi, x);
            vout[j] = hi;
        }

        cout << "Case #" << i << ":";
        for(auto e: vout){
            cout << ' ' << e;
        }
        cout << endl;
    }

    return 0;
}
