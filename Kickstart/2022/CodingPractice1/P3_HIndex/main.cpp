#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef vector<ll> vll;
typedef map<ll, ll> mllll;

ll sumCits(mllll m, ll i, ll hi){
    ll sum = 0;
    for(auto [key, val]: m){
        if(sum >= i){
            return sum;
        }
        if(key>=i){
            sum += val;
            continue;
        }
        if(i<=hi){
            m.erase(key);
            continue;
        }
    }
    return sum;
}

void printvll(vll vin){
    for(auto x: vin){
        cout << ' ' << x;
    }
}

int main(){
    ll T, N;
    vll vout, vin;

    ll hi, nart, x;
    mllll m;

    cin >> T;

    for(ll i=1; i<=T; i++){
        cin >> N;
        vout = vll();
        hi = 1;
        m = mllll();

        for(ll j=1; j<=N; j++){
            cin >> x;
            if(x>hi){
                m[x]++;
            }else{
                vout.push_back(hi);
                continue;
            }

            for(ll i=hi+1; i<=j; i++){
                nart = sumCits(m, i, hi);
                if(nart>=hi){
                    hi = nart;
                }
            }

            vout.push_back(hi);
        }

        cout << "Case #" << i << ":";
        printvll(vout);
        cout << endl;
    }

    return 0;
}
