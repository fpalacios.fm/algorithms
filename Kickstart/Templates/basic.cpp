#include <iostream>

using namespace std;

typedef long long type_out;
typedef long long type_ntest;

type_out fun() {

	return 0;
}

int main() {
	type_ntest T;

	cin >> T;
	for (type_ntest x = 1; x <= T; x++) {

		type_out y = fun();

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
