#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef vector<int> vi;

vi fun(string s, int N) {
	int ml = 0;
	int x = 0;
	vi v;

	for (int i = 0; i < N; i++) {
		if (x + i == 0) {
			v.push_back(1);
		}
		else if (s[i - 1] < s[i]) {
			v.push_back(i - x + 1);
		}
		else {
			v.push_back(1);
			x = i;
		}
	}

	return v;
}

int main() {
	int T, N, x;
	string s;
	vi v;

	cin >> T;
	for (x = 1; x <= T; x++) {
		cin >> N >> s;
		v = fun(s, N);

		cout << "Case #" << x << ": ";
		
		for (auto y : v) {
			cout << y << " ";
		}

		cout << endl;
	}

	return 0;
}
