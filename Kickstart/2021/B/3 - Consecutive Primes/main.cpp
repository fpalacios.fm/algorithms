#include <iostream>
#include <vector>

using namespace std;

typedef unsigned int ui;
typedef long long ll;
typedef vector<ll> vui;

ui NP = 6;
ll initprimes[] = { 2,3,5,7,11,13 };
vui P(initprimes, initprimes + sizeof(initprimes) / sizeof(ll));

void is_prime(ll x) {
	for (auto p : P) {
		if (x%p==0) {
			return;
		}
	}

	P.push_back(x);
	NP++;
}

ll find_lin(ll N) {
	ll x = P[NP - 1];

	do {
		x += 2;
		is_prime(x);
	} while (P[NP-1]*P[NP-2]<=N);

	return P[NP-2] * P[NP-3];
}

ll find_log(ll N) {
	ui step = NP / 2, index = 1, aux;
	ll mp = P[index] * P[index - 1];

	while (step>0) {
		aux = index + step;
		while (aux<NP && P[aux]*P[aux-1]<=N) {
			index += step;
			aux += step;
		}
		step /= 2;
	}

	return P[index] * P[index - 1];
}

ll fun(ll N) {
	ll mp = 1;

	if (P[NP-1]*P[NP-2] < N) {
		mp = find_lin(N);
	}
	else {
		mp = find_log(N);
	}
	
	return mp;
}

int main() {
	ll T, N;

	cin >> T;
	for (ll x = 1; x <= T; x++) {
		cin >> N;

		ll y = fun(N);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
