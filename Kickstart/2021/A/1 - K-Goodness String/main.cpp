#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef vector<ll> vll;

int fun(int N, int K, string S){
    int sum = 0, diff;
    for(int i=0; i<N/2; i++){
        sum+=S[i]!=S[N-i-1];
    }
    diff = K-sum;
    return abs(diff);
}

int main(){
    int T, N, K, y;
    string S;

    cin >> T;
    for(int x=1; x<=T; x++){
        cin >> N >> K >> S;
        y = fun(N, K, S);
        cout << "Case #" << x << ": " << y << endl;
    }

    return 0;
}
