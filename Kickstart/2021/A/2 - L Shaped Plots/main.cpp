#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

const pair<int, int> UP(-1,0);
const pair<int, int> DOWN(1,0);
const pair<int, int> LEFT(0,-1);
const pair<int, int> RIGHT(0,1);

ll count(vector<vector<int>> v, int rows, int cols, int i, int j, pair<int, int> dir){
    ll c=0;

    do{
        c++;
        i+=dir.first;
        j+=dir.second;
    }while(0<=i && i<rows && 0<=j && j<cols && v[i][j]==1);

    return c;
}

ll min(ll x, ll y){
    return x<y?x:y;
}

ll LSubCount(ll l, ll s){
    ll ans = min(l/2-1, s-1);
    return ans>=0?ans:0;
}

ll LCount(vector<vector<int>> v, int rows, int cols, int i, int j){
    ll c_up, c_down, c_left, c_right, L=0;
    c_up = count(v, rows, cols, i, j, UP);
    c_down = count(v, rows, cols, i, j, DOWN);
    c_left = count(v, rows, cols, i, j, LEFT);
    c_right = count(v, rows, cols, i, j, RIGHT);

    // cout << i << "," << j << " ^" << c_up << " v" << c_down << " <" << c_left << " >" << c_right << endl;

    L += LSubCount(c_up, c_right) + LSubCount(c_right, c_up);
    // cout << " ^>" << LSubCount(c_up, c_right);
    // cout << " >^" << LSubCount(c_right, c_up);
    L += LSubCount(c_up, c_left) + LSubCount(c_left, c_up);
    // cout << " ^<" << LSubCount(c_up, c_left);
    // cout << " <^" << LSubCount(c_left, c_up);
    L += LSubCount(c_down, c_right) + LSubCount(c_right, c_down);
    // cout << " v>" << LSubCount(c_down, c_right);
    // cout << " >v" << LSubCount(c_right, c_down);
    L += LSubCount(c_down, c_left) + LSubCount(c_left, c_down);
    // cout << " v<" << LSubCount(c_down, c_left);
    // cout << " <v" << LSubCount(c_left, c_down) << endl;
    return L;
}

ll fun(vector<vector<int>> v, int rows, int cols){
    ll suma = 0;
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            if(v[i][j]==1){
                suma+=LCount(v, rows, cols, i, j);
            }
        }
    }

    return suma;
}

int main(){
    int T, rows, cols;
    ll y;

    cin >> T;
    for(int x=1; x<=T; x++){
        cin >> rows >> cols;
        vector<vector<int>> v(rows, vector<int>(cols));

        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                cin >> v[i][j];
            }
        }

        y = fun(v, rows, cols);
        cout << "Case #" << x << ": " << y << endl;
    }

    return 0;
}
