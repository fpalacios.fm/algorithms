#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

const pair<int, int> UP(-1,0);
const pair<int, int> DOWN(1,0);
const pair<int, int> LEFT(0,-1);
const pair<int, int> RIGHT(0,1);

ll LSubCount(ll l, ll s){
    ll ans = min(l/2-1, s-1);
    return ans>=0?ans:0;
}

ll LCount(vector<vector<int>> v, vector<vector<int>> u_up, vector<vector<int>> u_left, vector<vector<int>> u_down, vector<vector<int>> u_right, int rows, int cols, int i, int j){
    ll c_up, c_down, c_left, c_right, L=0;
    c_up = u_up[i][j] + v[i][j];
    c_down = u_down[i][j] + v[i][j];
    c_left = u_left[i][j] + v[i][j];
    c_right = u_right[i][j] + v[i][j];

    // cout << i << "," << j << " ^" << c_up << " v" << c_down << " <" << c_left << " >" << c_right << endl;

    L += LSubCount(c_up, c_right) + LSubCount(c_right, c_up);
    // cout << " ^>" << LSubCount(c_up, c_right);
    // cout << " >^" << LSubCount(c_right, c_up);
    L += LSubCount(c_up, c_left) + LSubCount(c_left, c_up);
    // cout << " ^<" << LSubCount(c_up, c_left);
    // cout << " <^" << LSubCount(c_left, c_up);
    L += LSubCount(c_down, c_right) + LSubCount(c_right, c_down);
    // cout << " v>" << LSubCount(c_down, c_right);
    // cout << " >v" << LSubCount(c_right, c_down);
    L += LSubCount(c_down, c_left) + LSubCount(c_left, c_down);
    // cout << " v<" << LSubCount(c_down, c_left);
    // cout << " <v" << LSubCount(c_left, c_down) << endl;
    return L;
}

ll fun(vector<vector<int>> v, vector<vector<int>> u_up, vector<vector<int>> u_left, vector<vector<int>> u_down, vector<vector<int>> u_right, int rows, int cols){
    ll suma = 0;
    for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
            if(v[i][j]==1){
                suma+=LCount(v, u_up, u_left, u_down, u_right, rows, cols, i, j);
            }
        }
    }

    return suma;
}

int main(){
    int T, rows, cols;
    ll y, si=0, sj=0, aux;

    cin >> T;
    for(int x=1; x<=T; x++){
        cin >> rows >> cols;

        vector<vector<int>> v(rows, vector<int>(cols));
        vector<vector<int>> u_up(rows, vector<int>(cols, 0));
        vector<vector<int>> u_left(rows, vector<int>(cols, 0));
        vector<vector<int>> u_down(rows, vector<int>(cols, 0));
        vector<vector<int>> u_right(rows, vector<int>(cols, 0));

        for(int i=0; i<rows; i++){
            for(int j=0; j<cols; j++){
                cin >> v[i][j];

                u_up[i][j] = 0;
                u_up[i][j] += i>0 ? v[i-1][j]*(u_up[i-1][j] + 1) : 0;

                u_left[i][j] = 0;
                u_left[i][j] += j>0 ? v[i][j-1]*(u_left[i][j-1] + 1) : 0;
            }
        }

        for(int i=rows-1; i>=0; i--){
            for(int j=cols-1; j>=0; j--){
                u_down[i][j] = 0;
                u_down[i][j] += i<rows-1 ? v[i+1][j]*(u_down[i+1][j] + 1) : 0;

                u_right[i][j] = 0;
                u_right[i][j] += j<cols-1 ? v[i][j+1]*(u_right[i][j+1] + 1) : 0;
            }
        }

        y = fun(v, u_up, u_left, u_down, u_right, rows, cols);
        cout << "Case #" << x << ": " << y << endl;
    }

    return 0;
}
