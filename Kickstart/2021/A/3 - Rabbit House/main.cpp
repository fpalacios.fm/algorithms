#include <iostream>
#include <string>
#include <vector>
#include <stack>

using namespace std;

typedef vector<vector<int>> mi;
typedef pair<int, int> pii;
typedef stack<pii> spii;
typedef long long ll;

bool validOrd(int i, int R) {
	return 0 <= i && i < R;
}

bool validAbs(int j, int C) {
	return 0 <= j && j < C;
}

bool validCoord(int i, int j, int R, int C) {
	return validOrd(i, R) && validAbs(j, C);
}

void fix(mi &G, int R, int C, int i, int j, spii &S, ll &suma) {
	S.push(pii(i, j));
	int aux;

	while (S.size() > 0) {
		pii p = S.top();
		i = p.first;
		j = p.second;
		S.pop();

		if (validOrd(i - 1, R)) {
			aux = G[i][j] - G[i - 1][j];
			if (aux > 1) {
				G[i - 1][j] += aux - 1;
				suma += aux - 1;
				S.push(pii(i - 1, j));
			}
		}

		if (validOrd(i + 1, R)) {
			aux = G[i][j] - G[i + 1][j];
			if (aux > 1) {
				G[i + 1][j] += aux - 1;
				suma += aux - 1;
				S.push(pii(i + 1, j));
			}
		}

		if (validAbs(j - 1, C)) {
			aux = G[i][j] - G[i][j - 1];
			if (aux > 1) {
				G[i][j - 1] += aux - 1;
				suma += aux - 1;
				S.push(pii(i, j - 1));
			}
		}

		if (validAbs(j + 1, C)) {
			aux = G[i][j] - G[i][j + 1];
			if (aux > 1) {
				G[i][j + 1] += aux - 1;
				suma += aux - 1;
				S.push(pii(i, j + 1));
			}
		}
	}
}

ll fun(mi &G, int R, int C) {
	spii S;
	ll suma = 0;
	for (int i = 0; i < R; i++) {
		for (int j = 0; j < C; j++) {
			fix(G, R, C, i, j, S, suma);
		}
	}
	return suma;
}

int main() {
	int T, x, R, C;
	ll y;

	cin >> T;
	for (x = 1; x <= T; x++) {
		cin >> R >> C;
		mi G(R, vector<int>(C));

		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				cin >> G[i][j];
			}
		}

		y = fun(G, R, C);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
