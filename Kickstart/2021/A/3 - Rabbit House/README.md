### Problem
Barbara got really good grades in school last year, so her parents decided to gift her with a pet rabbit. She was so excited that she built a house for the rabbit, which can be seen as a 2D grid with `R` rows and `C` columns.

Rabbits love to jump, so Barbara stacked several boxes on several cells of the grid. Each box is a cube with equal dimensions, which match exactly the dimensions of a cell of the grid.

However, Barbara soon realizes that it may be dangerous for the rabbit to make jumps of height greater than `1` box, so she decides to avoid that by making some adjustments to the house. For every pair of adjacent cells, Barbara would like that their absolute difference in height be at most `1` box. Two cells are considered adjacent if they share a common side.

As all the boxes are superglued, Barbara cannot remove any boxes that are there initially, however she can add boxes on top of them. She can add as many boxes as she wants, to as many cells as she wants (which may be zero). Help her determine what is the minimum total number of boxes to be added so that the rabbit's house is safe.

### Input
The first line of the input gives the number of test cases, `T`. `T` test cases follow.

Each test case begins with a line containing two integers `R` and `C`.

Then, `R` lines follow, each with `C` integers. The `j`-th integer on `i`-th line, $`G_{i,j}`$, represents how many boxes are there initially on the cell located at the `i`-th row and `j`-th column of the grid.

### Output
For each test case, output one line containing `Case #x: y`, where `x` is the test case number (starting from `1`) and `y` is the minimum number of boxes to be added so that the rabbit's house is safe.

### Limits
- Memory limit: `1 GB`.
- $`1 \leq T \leq 100`$.
- $`0 \leq G_{i,j} \leq 2 \cdot 10^6, \; \forall i, j`$.

### Test Set 1
- Time limit: `20 seconds`.
- $`1 \leq R,C \leq 50`$.

### Test Set 2
- Time limit: `40 seconds`.
- $`1 \leq R,C \leq 300`$.

### Sample
|Sample Input|Sample Output|
|---|---|
|3<br>1 3<br>3 4 3<br>1 3<br>3 0 0<br>3 3<br>0 0 0<br>0 2 0<br>0 0 0|Case #1: 0<br>Case #2: 3<br>Case #3: 4|

<table class="table table-bordered" style="text-align:center;">
<tbody><tr>
<th style="text-align:center;">Type</th>
<th style="text-align:center;">Typical Bit Width</th>
<th style="text-align:center;">Typical Range</th>
</tr>
<tr>
<td>char</td>
<td>1byte</td>
<td>-127 to 127 or 0 to 255</td>
</tr>
<tr>
<td>unsigned char</td>
<td>1byte</td>
<td>0 to 255</td>
</tr>
<tr>
<td>signed char</td>
<td>1byte</td>
<td>-127 to 127</td>
</tr>
<tr>
<td>int</td>
<td>4bytes</td>
<td>-2147483648 to 2147483647</td>
</tr>
<tr>
<td>unsigned int</td>
<td>4bytes</td>
<td>0 to 4294967295</td>
</tr>
<tr>
<td>signed int</td>
<td>4bytes</td>
<td>-2147483648 to 2147483647</td>
</tr>
<tr>
<td>short int</td>
<td>2bytes</td>
<td>-32768 to 32767</td>
</tr>
<tr>
<td>unsigned short int</td>
<td>2bytes</td>
<td>0 to 65,535</td>
</tr>
<tr>
<td>signed short int</td>
<td>2bytes</td>
<td>-32768 to 32767</td>
</tr>
<tr>
<td>long int</td>
<td>8bytes</td>
<td>-2,147,483,648 to 2,147,483,647</td>
</tr>
<tr>
<td>signed long int</td>
<td>8bytes</td>
<td>same as long int</td>
</tr>
<tr>
<td>unsigned long int</td>
<td>8bytes</td>
<td>0 to 4,294,967,295</td>
</tr>
<tr>
<td>long long int</td>
<td>8bytes</td>
<td>-(2^63) to (2^63)-1</td>
</tr>
<tr>
<td>unsigned long long int</td>
<td>8bytes</td>
<td>0 to 18,446,744,073,709,551,615</td>
</tr>
<tr>
<td>float</td>
<td>4bytes</td>
<td></td>
</tr>
<tr>
<td>double</td>
<td>8bytes</td>
<td></td>
</tr>
<tr>
<td>long double</td>
<td>12bytes</td>
<td></td>
</tr>
<tr>
<td>wchar_t</td>
<td>2 or 4 bytes</td>
<td>1 wide character</td>
</tr>
</tbody></table>
