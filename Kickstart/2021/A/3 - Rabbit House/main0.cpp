#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_set>

using namespace std;

typedef vector<vector<int>> mi;
typedef pair<int, int> pii;
typedef long long ll;

struct pii_hash {
	inline std::size_t operator()(const pii& v) const {
		return v.first * 31 + v.second;
	}
};

typedef unordered_set<pii, pii_hash> uspii;
typedef map<int, uspii, greater<int>> muspii;

bool validOrd(int i, int R) {
	return 0 <= i && i < R;
}

bool validAbs(int j, int C) {
	return 0 <= j && j < C;
}

bool validCoord(int i, int j, int R, int C) {
	return validOrd(i, R) && validAbs(j, C);
}

ll fix(int i, int j, int R, int C, int h, mi& G, muspii& Q) {
	if (validCoord(i, j, R, C)) {
		int g = G[i][j];
		int diff = h - g;
		if (diff > 1) {
			pii p(i, j);
			Q[g].erase(p);
			Q[h - 1].insert(p);
			G[i][j] = h - 1;
			return diff - 1;
		}
	}
	return 0;
}

ll fun(mi& G, muspii& Q, int R, int C) {
	ll boxes = 0;
	int i, j;

	for (auto&[h, S] : Q) {
		for (auto& p : S) {
			i = p.first;
			j = p.second;

			boxes += fix(i - 1, j, R, C, h, G, Q);
			boxes += fix(i + 1, j, R, C, h, G, Q);
			boxes += fix(i, j - 1, R, C, h, G, Q);
			boxes += fix(i, j + 1, R, C, h, G, Q);
		}
	}

	return boxes;
}

int main() {
	int T, x, R, C, h;
	ll y;
	pii p;

	cin >> T;
	for (x = 1; x <= T; x++) {
		cin >> R >> C;
		mi G(R, vector<int>(C));
		muspii Q;

		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				cin >> h;
				G[i][j] = h;
				Q[h].insert(pii(i, j));
			}
		}

			y = fun(G, Q, R, C);

			cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
