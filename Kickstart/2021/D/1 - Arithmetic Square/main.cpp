#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

typedef int ii;
typedef long long ll;
typedef vector<ll> vll;
typedef vector<vll> mll;
typedef unordered_map<ll, ii> mli;

ii get_max(mli& S) {
	ii m = 0;
	for (auto&[x, n] : S) {
		m = max(m, n);
	}
	return m;
}

ii aritma(ii x0, ii y0, ii x1, ii y1, ii x2, ii y2, mll& M) {
	ll m = M[x0][y0] + M[x2][y2];
	return m % 2 == 0 && m / 2 == M[x1][y1];
}

void eval(ii x0, ii y0, ii x2, ii y2, mll& M, mli& S) {
	ll x = M[x0][y0] + M[x2][y2];
	if (x % 2 == 0) {
		x /= 2;
		S[x]++;
	}
}

ii fun(mll& M) {
	mli S;
	ii count = 0;

	eval(1, 0, 1, 2, M, S);
	eval(0, 1, 2, 1, M, S);
	eval(0, 0, 2, 2, M, S);
	eval(0, 2, 2, 0, M, S);

	count += aritma(0, 0, 0, 1, 0, 2, M);
	count += aritma(2, 0, 2, 1, 2, 2, M);
	count += aritma(0, 0, 1, 0, 2, 0, M);
	count += aritma(0, 2, 1, 2, 2, 2, M);

	return count + get_max(S);
}

int main() {
	ii T;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		mll M(3, vll(3));
		cin >> M[0][0]
			>> M[0][1]
			>> M[0][2]
			>> M[1][0]
			>> M[1][2]
			>> M[2][0]
			>> M[2][1]
			>> M[2][2];

		ii y = fun(M);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
