#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

vector<bool> readBString(ll N){
    char c;
    vector<bool> v;

    for(ll i=0; i<N; i++){
        cin >> c;
        v.push_back(c=='1');
    }

    return v;
}

ll trashBins(ll N, vector<bool> v){
    ll suma = 0, x = -1, h, k;

    for(ll i=0; i<N;i++){
        if(v[i]){
            if(x==-1){
                suma+=i*(i+1)/2;
            }else{
                h = i-x-1;
                k = h/2;
                suma += (k+(h%2))*(k+1);
            }
            x = i;
        }else if(i==N-1){
            h = i-x;
            suma += h*(h+1)/2;
        }
    }

    return suma;
}

int main(){
    ll T, N;
    vector<bool> v;

    cin >> T;

    for(ll i=1; i<=T; i++){
        cin >> N;
        v = readBString(N);

        cout << "Case #" << i << ": " << trashBins(N, v) << endl;
    }

    return 0;
}
