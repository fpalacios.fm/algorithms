#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

using namespace std;

typedef long long ll;
typedef int ii;
typedef pair<char, ii> pci;
typedef vector<pci> vpci;
typedef map<char, ii> mci;

string fun(string s) {
	vpci V;
	mci M;
	int N = 0;

	for (auto c : s) {
		V.push_back(pci(c, N));
		M[c]++;
		N++;
	}

	int maxr = 0;
	for (auto[c, r] : M) {
		maxr = r > maxr ? r : maxr;
	}

	if (maxr > N / 2) {
		return "IMPOSSIBLE";
	}

	sort(V.begin(), V.end());

	int i, j;
	char aux;
	for (int k = 0; k < (N % 2 == 0 ? N / 2 : N / 2 + 1); k++) {
		i = V[k].second;
		j = V[k + N / 2].second;
		aux = s[i];
		s[i] = s[j];
		s[j] = aux;
	}

	return s;
}

int main() {
	ii T;
	string s;

	cin >> T;
	for (ii x = 1; x <= T; x++) {
		cin >> s;
		string y = fun(s);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
