#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;
typedef vector<char> vc;
typedef vector<vc> mc;
typedef vector<vector<pair<int, int>>> tpir;
typedef vector<tpir> tpi;

void initTRow(mc& F, tpi& T, int i, int M) {
	bool in_a_word = 0;
	int k, l;

	for (int j = 0; j <= M; j++) {
		if (j == M || F[i][j] == '#') {
			if (in_a_word) {
				l = j - k;
				for (int c = 0; c < l; c++) {
					if (k + c != k + l - c - 1) {
						T[i][k + c].push_back(pair<int, int>(i, k + l - c - 1));
					}
				}
			}
			in_a_word = 0;
		}
		else {
			if (!in_a_word) {
				k = j;
			}
			in_a_word = 1;
		}
	}
}

void initTCol(mc& F, tpi& T, int N, int j) {
	bool in_a_word = 0;
	int k, l;

	for (int i = 0; i <= N; i++) {
		if (i == N || F[i][j] == '#') {
			if (in_a_word) {
				l = i - k;
				for (int c = 0; c < l; c++) {
					if (k + c != k + l - c - 1) {
						T[k + c][j].push_back(pair<int, int>(k + l - c - 1, j));
					}
				}
			}
			in_a_word = 0;
		}
		else {
			if (!in_a_word) {
				k = i;
			}
			in_a_word = 1;
		}
	}
}

void initT(mc& F, tpi& T, int N, int M) {
	for (int i = 0; i < N; i++) {
		initTRow(F, T, i, M);
	}
	for (int j = 0; j < M; j++) {
		initTCol(F, T, N, j);
	}
}

void solve(mc& F, tpi& T, int i, int j, int& count) {
	if (F[i][j] != '.') {
		for (auto[ni, nj] : T[i][j]) {
			if (F[ni][nj] == '.') {
				F[ni][nj] = F[i][j];
				count++;
				solve(F, T, ni, nj, count);
			}
		}
	}
}

int fun(mc& F, int N, int M) {
	int count = 0;

	tpi T(N);
	for (int i = 0; i < N; i++) {
		T[i] = tpir(M);
	}

	initT(F, T, N, M);

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			solve(F, T, i, j, count);
		}
	}

	return count;
}

int main() {
	int T, N, M, y;
	mc F;
	cin >> T;

	for (int x = 1; x <= T; x++) {
		cin >> N >> M;

		F = mc(N);
		for (int i = 0; i < N; i++) {
			F[i] = vc(M);
			for (int j = 0; j < M; j++) {
				cin >> F[i][j];
			}
		}

		y = fun(F, N, M);

		cout << "Case #" << x << ": " << y << endl;

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				cout << F[i][j];
			}
			cout << endl;
		}
	}

	return 0;
}
