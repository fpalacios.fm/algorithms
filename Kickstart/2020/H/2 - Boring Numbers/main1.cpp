#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef long long ll;
typedef vector<ll> vll;
typedef vector<char> vc;

vll mem(18);

bool isBoring(string x) {
	char d;
	for (int i = 0; i < x.length(); i++) {
		d = x[i] - '0';
		if (d % 2 != (i + 1) % 2) {
			return false;
		}
	}
	return true;
}

ll boringsBelow(string X) {
	ll N = stoll(X);
	ll count = 0;

	for (ll i = 1; i < N; i++) {
		count += isBoring(to_string(i));
	}

	return count;
}

ll fun(string L, string R) {

	return boringsBelow(R) - boringsBelow(L) + isBoring(R);
}

int main() {
	int T;
	ll y;
	string L, R;
	cin >> T;

	for (int x = 1; x <= T; x++) {
		cin >> L >> R;
		y = fun(L, R);

		cout << "Case #" << x << ": " << y << endl;
	}

	return 0;
}
